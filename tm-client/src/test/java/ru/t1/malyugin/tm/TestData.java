package ru.t1.malyugin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.*;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.ITokenService;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.service.PropertyService;
import ru.t1.malyugin.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

public interface TestData {

    @NotNull
    ITokenService TOKEN_SERVICE_USUAL = new TokenService();

    @NotNull
    ITokenService TOKEN_SERVICE_ADMIN = new TokenService();

    @NotNull
    IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    ITaskEndpoint TASK_ENDPOINT = ITaskEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    ISystemEndpoint SYSTEM_ENDPOINT = ISystemEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    @NotNull
    List<TaskDTO> TASK_LIST = new ArrayList<>();

}