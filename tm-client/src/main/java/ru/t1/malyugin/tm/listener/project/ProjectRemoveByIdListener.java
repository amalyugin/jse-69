package ru.t1.malyugin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove project by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        projectEndpoint.removeProjectById(request);
    }

}