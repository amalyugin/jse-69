package ru.t1.malyugin.tm.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.client.ProjectRestEndpointClient;
import ru.t1.malyugin.tm.client.ProjectsRestEndpointClient;
import ru.t1.malyugin.tm.marker.IntegrationCategory;
import ru.t1.malyugin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectsTest {

    private static final ProjectsRestEndpointClient PROJECTS_CLIENT = ProjectsRestEndpointClient.getInstance();
    private static final ProjectRestEndpointClient PROJECT_CLIENT = ProjectRestEndpointClient.getInstance();
    private final List<Project> projects = new ArrayList<>();
    private final Project project1 = new Project("P1", "D1");
    private final Project project2 = new Project("P2", "D2");

    @Before
    public void before() {
        projects.add(project1);
        projects.add(project2);
        PROJECT_CLIENT.post(project1);
        PROJECT_CLIENT.post(project2);
    }

    @After
    public void after() {
        projects.clear();
        PROJECTS_CLIENT.clear();
    }

    @Test
    public void getTest() {
        final Iterable<Project> projectList = PROJECTS_CLIENT.get();
        Assert.assertNotNull(projectList);
    }

    @Test
    public void countTest() {
        Assert.assertEquals(projects.size(), PROJECTS_CLIENT.count());
        final Project project = new Project("NEW", "NEW");
        PROJECT_CLIENT.post(project);
        Assert.assertEquals(projects.size() + 1, PROJECTS_CLIENT.count());
    }

    @Test
    public void deleteAllTest() {
        long count = PROJECTS_CLIENT.count();
        final Project p1 = new Project("1", "1");
        final Project p2 = new Project("2", "2");
        final Project p3 = new Project("3", "3");
        final List<Project> toAdd = new ArrayList<>();
        toAdd.add(p1);
        toAdd.add(p2);
        toAdd.add(p3);
        PROJECT_CLIENT.post(p1);
        PROJECT_CLIENT.post(p2);
        PROJECT_CLIENT.post(p3);
        count = count + 3;
        Assert.assertEquals(count, PROJECTS_CLIENT.count());
        PROJECTS_CLIENT.delete(toAdd);
        count = count - 3;
        Assert.assertEquals(count, PROJECTS_CLIENT.count());
    }

}