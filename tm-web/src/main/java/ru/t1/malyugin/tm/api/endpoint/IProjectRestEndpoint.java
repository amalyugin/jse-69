package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    @GetMapping("/get/{id}")
    Project getById(
            @PathVariable final String id
    );

    @PostMapping("/post")
    void create(
            @RequestBody final Project project
    );

    @PutMapping("/put")
    void update(
            @RequestBody final Project project
    );

    @DeleteMapping("/delete/{id}")
    void deleteById(
            @PathVariable final String id
    );

    @GetMapping("/get")
    Collection<Project> getList();

    @GetMapping("/count")
    long count();

    @DeleteMapping("/delete/all")
    void clearList();

}