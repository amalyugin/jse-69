package ru.t1.malyugin.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.UserUtil;

import java.util.Collection;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/get/{id}")
    public Task getById(
            @PathVariable("id") final String id
    ) {
        return taskService.findByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/post")
    public void create(
            @RequestBody final Task task
    ) {
        taskService.addForUser(UserUtil.getUserId(), task);
    }

    @Override
    @PutMapping("/put")
    public void update(
            @RequestBody final Task task
    ) {
        taskService.editForUser(UserUtil.getUserId(), task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @PathVariable("id") final String id
    ) {
        taskService.deleteByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/get")
    public Collection<Task> getList() {
        return taskService.findAllForUser(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return (int) taskService.countForUser(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clearList() {
        taskService.clearForUser(UserUtil.getUserId());
    }

}