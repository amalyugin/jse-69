package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectSoapEndpoint {

    @WebMethod
    Project getById(
            @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void create(
            @WebParam(name = "project", partName = "project") Project project
    );

    @WebMethod
    void update(
            @WebParam(name = "project", partName = "project") Project project
    );

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    Collection<Project> getList();

    @WebMethod
    long count();

    @WebMethod
    void clearList();

}