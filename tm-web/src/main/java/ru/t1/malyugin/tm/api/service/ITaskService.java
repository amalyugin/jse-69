package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Task;

import java.util.Collection;

public interface ITaskService {

    Collection<Task> findAll();

    Collection<Task> findAllForUser(String userId);

    long count();

    long countForUser(String userId);

    void create();

    void createForUser(String userId);

    void add(Task task);

    void addForUser(String userId, Task task);

    void deleteById(String id);

    void deleteByIdForUser(String userId, String id);

    void clear();

    void clearForUser(String userId);

    void edit(Task task);

    void editForUser(String userId, Task task);

    Task findById(String id);

    Task findByIdForUser(String userId, String id);

}