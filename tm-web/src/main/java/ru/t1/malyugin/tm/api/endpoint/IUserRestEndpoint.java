package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.User;

import java.util.Collection;

@RequestMapping("/api/user")
public interface IUserRestEndpoint {

    @PostMapping("/create")
    void create(
            @RequestBody final User user
    );

    @PutMapping("/edit")
    void edit(
            @RequestBody final User user
    );

    @GetMapping("/get/{id}")
    User find(
            @PathVariable final String id
    );

    @GetMapping("/get")
    Collection<User> getList();

}