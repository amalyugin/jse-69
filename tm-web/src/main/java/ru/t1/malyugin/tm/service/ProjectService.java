package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.Random;

@Service
public class ProjectService implements IProjectService {

    private final Random random = new Random();
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Collection<Project> findAllForUser(final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public long countForUser(final String userId) {
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void create() {
        int randomNumber = random.nextInt(101);
        Project project = new Project("P " + randomNumber, "DESC");
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void createForUser(final String userId) {
        int randomNumber = random.nextInt(101);
        Project project = new Project("P " + randomNumber, "DESC");
        project.setUserId(userId);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void add(final Project project) {
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void addForUser(final String userId, final Project project) {
        project.setUserId(userId);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        if (!projectRepository.existsById(id)) return;
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByIdForUser(final String userId, final String id) {
        if (!projectRepository.existsByUserIdAndId(userId, id)) return;
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clearForUser(final String userId) {
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void edit(final Project project) {
        if (!projectRepository.existsById(project.getId())) return;
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void editForUser(final String userId, final Project project) {
        if (!projectRepository.existsByUserIdAndId(userId, project.getId())) return;
        projectRepository.save(project);
    }

    @Override
    public Project findById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public Project findByIdForUser(final String userId, final String id) {
        return projectRepository.findByUserIdAndId(userId, id).orElse(null);
    }

}