package ru.t1.malyugin.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.UserUtil;

import java.util.Collection;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/get/{id}")
    public Project getById(
            @PathVariable("id") final String id
    ) {
        return projectService.findByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/post")
    public void create(
            @RequestBody final Project project
    ) {
        projectService.addForUser(UserUtil.getUserId(), project);
    }

    @Override
    @PutMapping("/put")
    public void update(
            @RequestBody final Project project
    ) {
        projectService.editForUser(UserUtil.getUserId(), project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @PathVariable("id") final String id
    ) {
        projectService.deleteByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/get")
    public Collection<Project> getList() {
        return projectService.findAllForUser(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return (int) projectService.countForUser(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clearList() {
        projectService.clearForUser(UserUtil.getUserId());
    }

}