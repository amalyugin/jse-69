package ru.t1.malyugin.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Task;

@FeignClient(name = "taskClient")
public interface TaskRestEndpointClient {

    String URL = "http://localhost:8080/api/task";

    static TaskRestEndpointClient getInstance() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, URL);
    }

    @GetMapping("/get/{id}")
    Task get(@PathVariable("id") String id);

    @PostMapping("/post")
    void post(@RequestBody Task task);

    @PutMapping("/put")
    void put(@RequestBody Task task);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}