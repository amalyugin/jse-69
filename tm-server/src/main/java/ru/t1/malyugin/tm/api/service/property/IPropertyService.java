package ru.t1.malyugin.tm.api.service.property;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider, IDatabasePropertyService, IApplicationPropertyService,
        ISessionPropertyService {

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getJmsQueueName();

    @NotNull
    String getJmsQueueUrl();

}