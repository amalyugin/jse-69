package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.AccessDeniedException;
import ru.t1.malyugin.tm.exception.user.UserAuthException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.util.CryptUtil;
import ru.t1.malyugin.tm.util.HashUtil;

import java.util.Date;

@Service
public final class AuthService implements IAuthService {

    @NotNull
    private final IUserDTOService userDTOService;

    @NotNull
    private final ISessionDTOService sessionDTOService;

    @NotNull
    private final IPropertyService propertyService;

    @Autowired
    public AuthService(
            @NotNull final IUserDTOService userDTOService,
            @NotNull final ISessionDTOService sessionDTOService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userDTOService = userDTOService;
        this.sessionDTOService = sessionDTOService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        userDTOService.create(login, password, email, null);
        @Nullable final UserDTO user = userDTOService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return getToken(user);
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userDTOService.findOneByLogin(login.trim());
        if (user == null) throw new UserAuthException();
        if (user.getLocked()) throw new UserAuthException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new UserAuthException();
        if (!StringUtils.equals(hash, user.getPasswordHash())) throw new UserAuthException();
        return getToken(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        if (sessionDTOService.findOneById(session.getId()) == null) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable final SessionDTO session) {
        if (session == null) return;
        sessionDTOService.removeById(session.getUserId(), session.getId());
    }

    @NotNull
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        sessionDTOService.add(session);
        return session;
    }

}