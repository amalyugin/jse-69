package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.dto.IWBSDTOService;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.repository.dto.AbstractWBSDTORepository;

import java.util.List;

public abstract class AbstractWBSDTOService<M extends AbstractWBSDTOModel> extends AbstractUserOwnedDTOService<M>
        implements IWBSDTOService<M> {

    @NotNull
    @Override
    protected abstract AbstractWBSDTORepository<M> getRepository();

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final EntitySort sort
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (sort == null) return getRepository().findAllByUserId(userId.trim());
        return getRepository().findAllByUserId(userId.trim(), Sort.by(Sort.Direction.ASC, sort.getName()));
    }

    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return;
        if (status != null) model.setStatus(status);
        update(model);
    }

}