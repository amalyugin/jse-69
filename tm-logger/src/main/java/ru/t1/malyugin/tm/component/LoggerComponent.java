package ru.t1.malyugin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.IPropertyService;

import javax.jms.*;

@Component
public final class LoggerComponent {

    @Autowired
    public LoggerComponent(
            @NotNull final MessageListener entityListener,
            @NotNull final ConnectionFactory connectionFactory,
            @NotNull final IPropertyService propertyService
    ) throws JMSException {
        @NotNull final String queueName = propertyService.getQueueName();
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(queueName);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}